package com.kenfogel.popup_demo.persistence;

import com.kenfogel.popup_demo.beans.FishData;
import java.sql.SQLException;

/**
 * Interface for CRUD methods
 *
 * @author Ken
 */
public interface FishV2DAO {

    public FishData findPrevByID(FishData fishData) throws SQLException;

    public FishData findNextByID(FishData fishData) throws SQLException;

    public int saveFish(FishData fishData) throws SQLException;
}
