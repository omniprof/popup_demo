/**
 * Read Fish records sequentially by ID in both directions
 *
 * @author Ken Fogel
 * @version 1.8
 */
package com.kenfogel.popup_demo.persistence;

import com.kenfogel.popup_demo.beans.FishData;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the FishDAO interface
 *
 * Exceptions are possible whenever a JDBC object is used. When these exceptions
 * occur it should result in some message appearing to the user and possibly
 * some corrective action. To simplify this, all exceptions are thrown and not
 * caught here. The methods that you write to call any of these methods must
 * either use a try/catch or continue throwing the exception.
 *
 * @author Ken
 * @version 1.0
 *
 */
public class FishV2DAOImpl implements FishV2DAO {

    private final static Logger LOG = LoggerFactory.getLogger(FishV2DAOImpl.class);

    private final String url = "jdbc:mysql://localhost:3306/AQUARIUM?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final String user = "fish";
    private final String password = "kfstandard";

    public FishV2DAOImpl() {
        super();
    }

    /**
     * Retrieve the next record from the given table based on the current
     * primary key using a bound bean
     *
     * @param fishData
     * @return The FishData object
     * @throws java.sql.SQLException
     */
    @Override
    public FishData findNextByID(FishData fishData) throws SQLException {

        String selectQuery = "SELECT ID, COMMONNAME, LATIN, PH, KH, TEMP, FISHSIZE, SPECIESORIGIN, TANKSIZE, STOCKING, DIET FROM FISH WHERE ID = (SELECT MIN(ID) from FISH WHERE ID > ?)";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, fishData.getId());
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundFishData(resultSet, fishData);
                }
            }
        }
        LOG.info("Found " + fishData.getId());
        return fishData;
    }

    /**
     * Retrieve the previous record from the given table based on the current
     * primary key using a bound bean
     *
     * @param fishData
     * @return The FishData object
     * @throws java.sql.SQLException
     */
    @Override
    public FishData findPrevByID(FishData fishData) throws SQLException {

        String selectQuery = "SELECT ID, COMMONNAME, LATIN, PH, KH, TEMP, FISHSIZE, SPECIESORIGIN, TANKSIZE, STOCKING, DIET FROM FISH WHERE ID = (SELECT MAX(ID) from FISH WHERE ID < ?)";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, fishData.getId());
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundFishData(resultSet, fishData);
                }
            }
        }
        LOG.info("Found " + fishData.getId());
        return fishData;
    }

    /**
     * Gateway for either updating or saving
     *
     * @param fishData
     * @return
     * @throws SQLException
     */
    @Override
    public int saveFish(FishData fishData) throws SQLException {
        int records;
        if (fishData.getId() == -1) {
            records = createFish(fishData);
        } else {
            records = updateFish(fishData);
        }
        return records;
    }

    /**
     * Create a new fish
     *
     * @param fishData
     * @return
     * @throws SQLException
     */
    private int createFish(FishData fishData) throws SQLException {
        int records;
        String query = "INSERT INTO FISH (COMMONNAME, LATIN, PH, KH, TEMP, FISHSIZE, SPECIESORIGIN, TANKSIZE, STOCKING, DIET) values (?,?,?,?,?,?,?,?,?,?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            pStatement.setString(1, fishData.getCommonName());
            pStatement.setString(2, fishData.getLatin());
            pStatement.setString(3, fishData.getPh());
            pStatement.setString(4, fishData.getKh());
            pStatement.setString(5, fishData.getTemp());
            pStatement.setString(6, fishData.getFishSize());
            pStatement.setString(7, fishData.getSpeciesOrigin());
            pStatement.setString(8, fishData.getTemp());
            pStatement.setString(9, fishData.getStocking());
            pStatement.setString(10, fishData.getDiet());
            records = pStatement.executeUpdate();

            ResultSet rs = pStatement.getGeneratedKeys();
            int recordNum = -1;
            if (rs.next()) {
                recordNum = rs.getInt(1);
            }
            fishData.setId(recordNum);

        }
        return records;

    }

    /**
     * Update an existing fish
     *
     * @param fishData
     * @return
     * @throws SQLException
     */
    private int updateFish(FishData fishData) throws SQLException {
        int records;
        String query = "UPDATE fish SET COMMONNAME=?, LATIN=?, PH=?, KH=?, TEMP=?, FISHSIZE=?, SPECIESORIGIN=?, TANKSIZE=?, STOCKING=?, DIET=? WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setString(1, fishData.getCommonName());
            pStatement.setString(2, fishData.getLatin());
            pStatement.setString(3, fishData.getPh());
            pStatement.setString(4, fishData.getKh());
            pStatement.setString(5, fishData.getTemp());
            pStatement.setString(6, fishData.getFishSize());
            pStatement.setString(7, fishData.getSpeciesOrigin());
            pStatement.setString(8, fishData.getTemp());
            pStatement.setString(9, fishData.getStocking());
            pStatement.setString(10, fishData.getDiet());
            pStatement.setInt(11, fishData.getId());
            records = pStatement.executeUpdate();
        }
        return records;
    }

    /**
     * Fill an existing bean that is bound to a form
     *
     * @param resultSet
     * @param fishData
     * @return
     * @throws SQLException
     */
    private FishData createBoundFishData(ResultSet resultSet, FishData fishData) throws SQLException {
        fishData.setCommonName(resultSet.getString("COMMONNAME"));
        fishData.setDiet(resultSet.getString("DIET"));
        fishData.setKh(resultSet.getString("KH"));
        fishData.setLatin(resultSet.getString("LATIN"));
        fishData.setPh(resultSet.getString("PH"));
        fishData.setFishSize(resultSet.getString("FISHSIZE"));
        fishData.setSpeciesOrigin(resultSet.getString("SPECIESORIGIN"));
        fishData.setStocking(resultSet.getString("STOCKING"));
        fishData.setTankSize(resultSet.getString("TANKSIZE"));
        fishData.setTemp(resultSet.getString("TEMP"));
        fishData.setId(resultSet.getInt("ID"));
        return fishData;
    }

}
