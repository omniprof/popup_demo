package com.kenfogel.popup_demo;

import com.kenfogel.popup_demo.beans.FishData;
import com.kenfogel.popup_demo.controller.FishFormController;
import com.kenfogel.popup_demo.controller.MainLayoutController;
import com.kenfogel.popup_demo.persistence.FishV2DAO;
import com.kenfogel.popup_demo.persistence.FishV2DAOImpl;
import java.io.IOException;
import java.util.ResourceBundle;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This program displays a Stage with a menu and a toolbar. Selecting the choice
 * Form pops up a data entry form for the fish.
 *
 * @author Ken
 */
public class MainApp extends Application {

    // slf4j log4j logger
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private Stage primaryStage;
    private Parent rootPane;
    private DialogPane formDialog;
    private Dialog dialog;

    // Shared Data
    private final FishV2DAO fishDAO;
    private final FishData fishData;

    /**
     * Default constructor that instantiates the DAO object. This is done here
     * rather than in the fxml controller so that it can be shared in other
     * controllers if they existed. Optionally, being used to support changing
     * the locale to see if i18n is functioning. Methods that access the
     * resource bundles directly are overloaded to use a Locale object
     */
    public MainApp() {
        super();
        // These are the shared objects
        fishDAO = new FishV2DAOImpl();
        fishData = new FishData();
    }

    /**
     * All JavaFX programs must override start and receive the Stage object from
     * the framework. After decorating the Stage it calls upon another method to
     * create the Scene.
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("TITLE"));

        // Set the application icon using getResourceAsStream because the image
        // file is in the jar.
        this.primaryStage.getIcons().add(
                new Image(MainApp.class
                        .getResourceAsStream("/images/bluefish_icon.png")));

        initRootLayout();
        primaryStage.show();
    }

    /**
     * The stop method is called before the stage is closed. You can use this
     * method to perform any actions that must be carried out before the program
     * ends. The JavaFX GUI is still running. The only action you cannot perform
     * is to cancel the Platform.exit() that led to this method.
     */
    @Override
    public void stop() {
        log.info("Stage is closing");
    }

    /**
     * Load the layout and controller for both the main Scene and the Dialog.
     * The main scene receives a reference to the Dialog so that it can display
     * it. The fish bean and DAO are also passed to the Scene and Dialog.
     */
    public void initRootLayout() {

        try {
            // Instantiate a FXMLLoader object
            FXMLLoader loader = new FXMLLoader();

            // Configure the FXMLLoader with the i18n locale resource bundles
            loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
            // Connect the FXMLLoader to the fxml file that is stored in the jar
            loader.setLocation(MainApp.class
                    .getResource("/fxml/FishForm.fxml"));

            // The load command returns a reference to the root pane of the fxml file
            formDialog = (DialogPane) loader.load();

            // Retrive a refernce to the controller from the FXMLLoader
            FishFormController controller = loader.getController();

            // Set the DAO object in the controller
            controller.setFishDAOData(fishData, fishDAO);

            // Create a Dialog and place the DialogPane inside it.
            dialog = new Dialog();
            dialog.setDialogPane(formDialog);

        } catch (IOException ex) {
            log.error("Error creating the dialog", ex);
            errorAlert(ex.getMessage());
            Platform.exit();
        }

        // Load the main app fxml
        try {
            // Instantiate a FXMLLoader object
            FXMLLoader loader = new FXMLLoader();

            // Configure the FXMLLoader with the i18n locale resource bundles
            loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
            // Connect the FXMLLoader to the fxml file that is stored in the jar
            loader.setLocation(MainApp.class
                    .getResource("/fxml/MainLayout.fxml"));

            // The load command returns a reference to the root pane of the fxml file
            rootPane = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootPane);

            // Put the Scene on the Stage
            primaryStage.setScene(scene);

            // Retrive a refernce to the controller from the FXMLLoader
            MainLayoutController controller = loader.getController();

            controller.setDialog(dialog);

        } catch (IOException ex) {
            log.error("Error creating the stage", ex);
            errorAlert(ex.getMessage());
            Platform.exit();
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("ERRORTITLE"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessagesBundle").getString("ERRORTITLE"));
        dialog.setContentText(msg);
        dialog.show();
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}
