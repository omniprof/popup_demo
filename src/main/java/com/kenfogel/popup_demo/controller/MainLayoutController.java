package com.kenfogel.popup_demo.controller;

import com.kenfogel.popup_demo.beans.FishData;
import com.kenfogel.popup_demo.persistence.FishV2DAO;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Dialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainLayoutController {

    private final static Logger LOG = LoggerFactory.getLogger(MainLayoutController.class);

    // Database access
    private FishV2DAO fishDAO;
    private FishData fishData;

    private Dialog dialog;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML
    void handleClose(ActionEvent event) {
        displayDialog("CLOSE");
        Platform.exit();

    }

    @FXML
    void handleForm(ActionEvent event) {
//        displayDialog("FORM");
        dialog.showAndWait();

    }

    @FXML
    void handleTable(ActionEvent event) {
        displayDialog("TABLE");

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {

    }

    /**
     * Message popup dialog
     *
     * @param msg
     */
    private void displayDialog(String msg) {
        Alert dialogAlert = new Alert(Alert.AlertType.INFORMATION);
        dialogAlert.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("EVENTTITLE"));
        dialogAlert.setHeaderText(ResourceBundle.getBundle("MessagesBundle").getString("EVENTTITLE"));
        dialogAlert.setContentText(msg);
        dialogAlert.show();
    }

    /**
     * Sets a reference to the FishDAO object that retrieves data from the
     * database.
     *
     * @param fishData
     * @param fishDAO
     */
    public void setFishDataDAO(FishData fishData, FishV2DAO fishDAO) {
        this.fishDAO = fishDAO;
        this.fishData = fishData;
    }

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

}
